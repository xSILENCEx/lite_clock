import 'package:flutter/material.dart';
import 'package:lite_clock/util/app_util.dart';

///时钟背景
class ClockBackground extends StatelessWidget {
  final Widget child;

  const ClockBackground({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => LC.hideStatusBar(),
      child: Material(
        color: Theme.of(context).bottomAppBarColor,
        child: child,
      ),
    );
  }
}
