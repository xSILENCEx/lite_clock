import 'package:flutter/material.dart';

///滑动数字
class SlideNumber extends StatelessWidget {
  const SlideNumber({
    Key key,
    this.height = 200,
    this.width = 100,
    this.number = 0,
    this.style,
  }) : super(key: key);

  ///数字高度
  final double height;

  ///数字宽度
  final double width;

  ///显示的数字
  final int number;

  ///字体样式
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Stack(
        children: [
          AnimatedPositioned(
            curve: Curves.ease,
            duration: const Duration(milliseconds: 600),
            top: -(height * number).toDouble(),
            width: width,
            height: height * 10,
            child: ListView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: 10,
              itemBuilder: (c, index) {
                return Container(
                  height: height,
                  width: width,
                  alignment: Alignment.center,
                  child: Text(
                    '$index',
                    style: style ??
                        TextStyle(
                          color: Colors.white,
                          fontSize: 50,
                          fontWeight: FontWeight.bold,
                          height: 1,
                        ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
