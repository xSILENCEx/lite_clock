import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:lite_clock/util/app_util.dart';

///开关
class LcSwitch extends StatefulWidget {
  final Function(bool) onChange;

  ///设置描述
  final String info;

  ///默认状态
  final bool autoSelect;

  const LcSwitch({
    Key key,
    @required this.onChange,
    this.info = '',
    this.autoSelect = false,
  }) : super(key: key);

  @override
  _LcSwitchState createState() => _LcSwitchState();
}

class _LcSwitchState extends State<LcSwitch> {
  bool _on;

  @override
  void initState() {
    super.initState();
    _on = widget.autoSelect;
  }

  ///点击事件
  _onTap() {
    setState(() {
      _on = !_on;
    });

    if (widget.onChange != null) widget.onChange(_on);
  }

  @override
  Widget build(BuildContext context) {
    final double _switchSize = 200.px;
    final double _iconSize = 50.px;

    return FlatButton(
      padding: EdgeInsets.zero,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.px)),
      onPressed: _onTap,
      child: Container(
        width: _switchSize,
        height: _switchSize,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.px),
          border: Border.all(color: Colors.white.withOpacity(0.5), width: 2.px),
        ),
        child: Stack(
          children: [
            AnimatedPositioned(
              width: _on ? _iconSize + 20 : _iconSize,
              height: _iconSize,
              top: 20.px,
              left: _on ? 100.px : 20.px,
              curve: Curves.elasticOut,
              duration: const Duration(milliseconds: 1500),
              child: AnimatedContainer(
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 500),
                curve: Curves.ease,
                width: _on ? _iconSize + 20 : _iconSize,
                height: _iconSize,
                decoration: BoxDecoration(
                  color: _on ? Colors.green : Colors.transparent,
                  borderRadius: BorderRadius.circular(10.px),
                  border: Border.all(
                      color: _on ? Colors.green : Colors.red, width: 2.px),
                ),
                child: Stack(
                  children: [
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 300),
                      opacity: _on ? 0 : 1,
                      child: Icon(
                        Feather.x,
                        color: Colors.red,
                      ),
                    ),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 300),
                      opacity: _on ? 1 : 0,
                      child: Icon(
                        Feather.check,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.all(20.px),
                child: Text(
                  widget.info,
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.5),
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
