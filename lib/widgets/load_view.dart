import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'loaders/color_loader_4.dart';

import 'package:lite_clock/util/app_util.dart';

///加载视图
class LoadView extends StatefulWidget {
  const LoadView({
    Key key,
    @required this.onCancel,
  }) : super(key: key);

  final Function onCancel;

  @override
  _LoadViewState createState() => _LoadViewState();
}

class _LoadViewState extends State<LoadView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future.value(false);
      },
      child: Center(
        child: Container(
          width: 400.px,
          height: 400.px,
          decoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              const ColorLoader4(),
              Align(
                alignment: Alignment.topRight,
                child: FlatButton(
                  onPressed: () {},
                  child: Icon(
                    Ionicons.ios_close,
                    size: 80.px,
                    color: Theme.of(context).accentColor.withOpacity(0.8),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
