import 'package:flutter/material.dart';

import 'loader_dot.dart';

class ColorLoader5 extends StatefulWidget {
  final Duration duration;
  final DotType dotType;
  final Icon dotIcon;
  final double radius;
  final double size;

  const ColorLoader5({
    this.duration = const Duration(milliseconds: 1000),
    this.dotType = DotType.circle,
    this.dotIcon = const Icon(Icons.blur_on),
    this.radius = 2,
    this.size = 10,
  });

  @override
  _ColorLoader5State createState() => _ColorLoader5State();
}

class _ColorLoader5State extends State<ColorLoader5>
    with SingleTickerProviderStateMixin {
  Animation<double> _animationDot1;
  Animation<double> _animationDot2;
  Animation<double> _animationDot3;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(duration: widget.duration, vsync: this);

    _animationDot1 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.0, 0.70, curve: Curves.linear),
      ),
    );

    _animationDot2 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.1, 0.8, curve: Curves.linear),
      ),
    );

    _animationDot3 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.2, 0.9, curve: Curves.linear),
      ),
    );

    _loading();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _loading() async {
    try {
      await _controller.repeat().orCancel;
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AnimatedBuilder(
          animation: _controller,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Dot(
              radius: widget.radius,
              color: Theme.of(context).accentColor,
              type: widget.dotType,
              icon: widget.dotIcon,
              size: widget.size,
            ),
          ),
          builder: (c, child) {
            return Opacity(
              opacity: _animationDot1.value <= 0.4
                  ? 2.5 * _animationDot1.value
                  : (_animationDot1.value > 0.40 &&
                          _animationDot1.value <= 0.60)
                      ? 1.0
                      : 2.5 - (2.5 * _animationDot1.value),
              child: child,
            );
          },
        ),
        AnimatedBuilder(
          animation: _controller,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Dot(
              radius: widget.radius,
              color: Theme.of(context).accentColor,
              type: widget.dotType,
              icon: widget.dotIcon,
              size: widget.size,
            ),
          ),
          builder: (c, child) {
            return Opacity(
              opacity: _animationDot2.value <= 0.4
                  ? 2.5 * _animationDot2.value
                  : (_animationDot2.value > 0.40 &&
                          _animationDot2.value <= 0.60)
                      ? 1.0
                      : 2.5 - (2.5 * _animationDot2.value),
              child: child,
            );
          },
        ),
        AnimatedBuilder(
          animation: _controller,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Dot(
              radius: widget.radius,
              color: Theme.of(context).accentColor,
              type: widget.dotType,
              icon: widget.dotIcon,
              size: widget.size,
            ),
          ),
          builder: (c, child) {
            return Opacity(
              opacity: _animationDot3.value <= 0.4
                  ? 2.5 * _animationDot3.value
                  : (_animationDot3.value > 0.40 &&
                          _animationDot3.value <= 0.60)
                      ? 1.0
                      : 2.5 - (2.5 * _animationDot3.value),
              child: child,
            );
          },
        ),
      ],
    );
  }
}
