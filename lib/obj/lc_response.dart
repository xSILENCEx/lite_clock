import 'package:lite_clock/util/app_util.dart';

///通用响应
class LcResponse {
  int code;
  String msg;
  List<dynamic> newslist;

  LcResponse({this.code, this.msg, this.newslist});

  LcResponse.fromJson(Map<String, dynamic> j) {
    try {
      code = j['code'];
      msg = j['msg'];
      if (j['newslist'] != null) {
        newslist = List<Map>();

        j['newslist'].forEach((v) {
          newslist.add(v as Map);
        });
      }
    } catch (e) {
      LC.log('相应转换失败:$e', type: 2);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['code'] = this.code;
    data['msg'] = this.msg;
    if (this.newslist != null) {
      data['newslist'] = this.newslist.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
