import 'dart:convert';

import 'package:lite_clock/util/app_util.dart';

///农历类
class ChineseCalendar {
  String gregoriandate = '';
  String lunardate = '';
  String lunarFestival = '';
  String festival = '';
  String fitness = '';
  String taboo = '';
  String shenwei = '';
  String taishen = '';
  String chongsha = '';
  String suisha = '';
  String wuxingjiazi = '';
  String wuxingnayear = '';
  String wuxingnamonth = '';
  String xingsu = '';
  String pengzu = '';
  String jianshen = '';
  String tiangandizhiyear = '';
  String tiangandizhimonth = '';
  String tiangandizhiday = '';
  String lmonthname = '';
  String shengxiao = '';
  String lubarmonth = '';
  String lunarday = '';
  String jieqi = '';

  ChineseCalendar({
    this.gregoriandate = '',
    this.lunardate = '',
    this.lunarFestival = '',
    this.festival = '',
    this.fitness = '',
    this.taboo = '',
    this.shenwei = '',
    this.taishen = '',
    this.chongsha = '',
    this.suisha = '',
    this.wuxingjiazi = '',
    this.wuxingnayear = '',
    this.wuxingnamonth = '',
    this.xingsu = '',
    this.pengzu = '',
    this.jianshen = '',
    this.tiangandizhiyear = '',
    this.tiangandizhimonth = '',
    this.tiangandizhiday = '',
    this.lmonthname = '',
    this.shengxiao = '',
    this.lubarmonth = '',
    this.lunarday = '',
    this.jieqi = '',
  });

  ChineseCalendar.fromJson(Map<String, dynamic> j) {
    try {
      gregoriandate = j['gregoriandate'];
      lunardate = j['lunardate'];
      lunarFestival = j['lunar_festival'];
      festival = j['festival'];
      fitness = j['fitness'];
      taboo = j['taboo'];
      shenwei = j['shenwei'];
      taishen = j['taishen'];
      chongsha = j['chongsha'];
      suisha = j['suisha'];
      wuxingjiazi = j['wuxingjiazi'];
      wuxingnayear = j['wuxingnayear'];
      wuxingnamonth = j['wuxingnamonth'];
      xingsu = j['xingsu'];
      pengzu = j['pengzu'];
      jianshen = j['jianshen'];
      tiangandizhiyear = j['tiangandizhiyear'];
      tiangandizhimonth = j['tiangandizhimonth'];
      tiangandizhiday = j['tiangandizhiday'];
      lmonthname = j['lmonthname'];
      shengxiao = j['shengxiao'];
      lubarmonth = j['lubarmonth'];
      lunarday = j['lunarday'];
      jieqi = j['jieqi'];
    } catch (e) {
      LC.log('生成农历对象出错:$e');
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['gregoriandate'] = this.gregoriandate;
    data['lunardate'] = this.lunardate;
    data['lunar_festival'] = this.lunarFestival;
    data['festival'] = this.festival;
    data['fitness'] = this.fitness;
    data['taboo'] = this.taboo;
    data['shenwei'] = this.shenwei;
    data['taishen'] = this.taishen;
    data['chongsha'] = this.chongsha;
    data['suisha'] = this.suisha;
    data['wuxingjiazi'] = this.wuxingjiazi;
    data['wuxingnayear'] = this.wuxingnayear;
    data['wuxingnamonth'] = this.wuxingnamonth;
    data['xingsu'] = this.xingsu;
    data['pengzu'] = this.pengzu;
    data['jianshen'] = this.jianshen;
    data['tiangandizhiyear'] = this.tiangandizhiyear;
    data['tiangandizhimonth'] = this.tiangandizhimonth;
    data['tiangandizhiday'] = this.tiangandizhiday;
    data['lmonthname'] = this.lmonthname;
    data['shengxiao'] = this.shengxiao;
    data['lubarmonth'] = this.lubarmonth;
    data['lunarday'] = this.lunarday;
    data['jieqi'] = this.jieqi;

    return data;
  }

  @override
  String toString() {
    return json.encode(this.toJson());
  }
}
