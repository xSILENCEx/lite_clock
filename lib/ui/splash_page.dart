import 'package:flutter/material.dart';
import 'package:lite_clock/ui/splash_logo.dart';
import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/clock_background.dart';

///闪屏页
class SplashPage extends StatelessWidget {
  const SplashPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LC.initScreen(context);

    return WillPopScope(
        child: ClockBackground(
          child: Center(
            child: const SplashLogo(),
          ),
        ),
        onWillPop: () async => Future.value(false));
  }
}
