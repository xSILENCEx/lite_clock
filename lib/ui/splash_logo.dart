import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/animated_scale.dart';
import 'package:lite_clock/widgets/lm_icons.dart';
import 'package:permission_handler/permission_handler.dart';

import 'root_page.dart';

///闪屏页logo
class SplashLogo extends StatefulWidget {
  const SplashLogo({Key key}) : super(key: key);
  @override
  _SplashLogoState createState() => _SplashLogoState();
}

class _SplashLogoState extends State<SplashLogo> {
  bool _logoScale = true;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 0), () async {
      await _ready();
    });
  }

  ///准备
  _ready() async {
    setState(() {
      _logoScale = false;
    });

    bool _right = await LC.checkPermission(Permission.storage);
    _right = _right && await LC.checkPermission(Permission.notification);

    await Future.delayed(const Duration(milliseconds: 1500), () {});

    if (_right) {
      setState(() {
        _logoScale = true;
      });

      await Future.delayed(const Duration(milliseconds: 500), () {});

      await Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => const RootPage()),
        (_) => false,
      );
    } else {
      BlocProvider.of<TimeBloc>(context).stop();
      SystemNavigator.pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    LC.initScreen(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AnimatedScale(
          scale: _logoScale ? 0 : 1,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: Icon(
            LcIcons.logo,
            color: Theme.of(context).primaryColor,
            size: 90.px,
          ),
        ),
        SizedBox(width: 40.px),
        AnimatedContainer(
          height: 90.px,
          curve: Curves.ease,
          width: _logoScale ? 0 : 550.px,
          duration: const Duration(milliseconds: 500),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              Text(
                'LITE CLOCK',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 100.px,
                  fontWeight: FontWeight.bold,
                  height: 1,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
