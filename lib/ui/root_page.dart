import 'package:flutter/material.dart';
import 'package:lite_clock/ui/home/home_page.dart';
import 'package:lite_clock/ui/home/setting_page.dart';
import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/clock_background.dart';

///根节点
class RootPage extends StatefulWidget {
  const RootPage({Key key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    LC.initScreen(context);
    LC.log('build root page', type: 1);

    final List<Widget> _pages = [
      HomePage(
        onSetTap: () async {
          await _pageController.nextPage(
              duration: const Duration(milliseconds: 800), curve: Curves.ease);
        },
      ),
      SettingPage(
        onSetTap: () async {
          await _pageController.previousPage(
              duration: const Duration(milliseconds: 800), curve: Curves.ease);
        },
      ),
    ];

    return ClockBackground(
      child: PageView.builder(
        physics: LC.LcPhysics,
        controller: _pageController,
        itemCount: _pages.length,
        itemBuilder: (_, index) {
          return _pages[index];
        },
      ),
    );
  }
}
