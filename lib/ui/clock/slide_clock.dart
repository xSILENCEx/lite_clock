import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/widgets/slide_number.dart';
import 'package:lite_clock/util/app_util.dart';

import 'date_info.dart';

class SlideClock extends StatelessWidget {
  const SlideClock({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const DateInfo(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.hour != c.nowTime.hour,
              builder: (_, time) => _buildNum(context, time.nowTime.hour),
            ),
            _buildDot(context),
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.minute != c.nowTime.minute,
              builder: (_, time) => _buildNum(context, time.nowTime.minute),
            ),
            _buildDot(context),
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.second != c.nowTime.second,
              builder: (_, time) => _buildNum(context, time.nowTime.second),
            ),
          ],
        ),
      ],
    );
  }

  ///构建数字
  Widget _buildNum(BuildContext context, int number) {
    return Row(
      children: [
        _buildSlide(context, number ~/ 10),
        _buildSlide(context, number % 10),
      ],
    );
  }

  ///构建时钟项目
  Widget _buildSlide(BuildContext context, int number) {
    return SlideNumber(
      number: number,
      width: 110.px,
      style: TextStyle(
        fontFamily: 'UN',
        color: Theme.of(context).primaryColor,
        fontSize: 240.sp,
        fontWeight: FontWeight.bold,
        height: 1,
      ),
    );
  }

  ///构建分割点
  Widget _buildDot(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.px),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 14.px,
            height: 50.px,
            decoration: BoxDecoration(
              color: Theme.of(context).dividerColor,
              borderRadius: BorderRadius.circular(50),
            ),
          ),
          SizedBox(height: 30),
          Container(
            width: 14.px,
            height: 50.px,
            decoration: BoxDecoration(
              color: Theme.of(context).dividerColor,
              borderRadius: BorderRadius.circular(50),
            ),
          ),
        ],
      ),
    );
  }
}
