import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';

import 'package:lite_clock/util/app_util.dart';

import 'date_info.dart';

///常规时钟
class SimpleClock extends StatelessWidget {
  const SimpleClock({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const DateInfo(),
        DefaultTextStyle(
          style: TextStyle(
            fontSize: 200.px,
            color: Theme.of(context).primaryColor,
            fontFamily: 'Clock2',
            height: 1,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.hour != c.nowTime.hour,
                builder: (_, time) => _buildNum(context, time.nowTime.hour),
              ),
              _buildDot(context),
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.minute != c.nowTime.minute,
                builder: (_, time) => _buildNum(context, time.nowTime.minute),
              ),
              _buildDot(context),
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.second != c.nowTime.second,
                builder: (_, time) => _buildNum(context, time.nowTime.second),
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///构建数字
  _buildNum(BuildContext context, int number) {
    return Text('${number.toString().padLeft(2, '0')}');
  }

  ///构建分割点
  _buildDot(BuildContext context) {
    return Text(
      ':',
      style: TextStyle(color: Theme.of(context).dividerColor),
    );
  }
}
