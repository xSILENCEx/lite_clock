import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/app_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/obj/chinese_calendar.dart';
import 'package:lite_clock/widgets/animated_roate.dart';

import 'round_num.dart';

import 'package:lite_clock/util/app_util.dart';

///环形时钟
class RoundClock extends StatelessWidget {
  const RoundClock({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double _maskHeight = 220.px;

    return Transform.translate(
      offset: Offset(-200.px, 0),
      child: Transform.scale(
        scale: 1,
        child: Stack(
          alignment: Alignment.center,
          children: [
            //秒
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.second != c.nowTime.second,
              builder: (_, time) =>
                  _buildNum(context, time.nowTime.second, RoundType.sed),
            ),
            //分
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.minute != c.nowTime.minute,
              builder: (_, time) =>
                  _buildNum(context, time.nowTime.minute, RoundType.min),
            ),
            //时
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.hour != c.nowTime.hour,
              builder: (_, time) =>
                  _buildNum(context, time.nowTime.hour, RoundType.hour),
            ),
            //日
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.day != c.nowTime.day,
              builder: (_, time) =>
                  _buildNum(context, time.nowTime.day, RoundType.day),
            ),
            //月
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.month != c.nowTime.month,
              builder: (_, time) =>
                  _buildNum(context, time.nowTime.month, RoundType.month),
            ),

            ///遮罩
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: double.infinity,
                height: _maskHeight,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.black,
                      Colors.black54,
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: _maskHeight,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Colors.black,
                      Colors.black54,
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                width: 360.px,
                height: MediaQuery.of(context).size.height - _maskHeight * 2,
                color: Colors.black54,
              ),
            ),

            ///年份
            BlocBuilder<TimeBloc, TimeMag>(
              buildWhen: (p, c) => p.nowTime.year != c.nowTime.year,
              builder: (_, time) => _buildNum(
                  context, time.nowTime.year, RoundType.year,
                  chTime: time.calendar),
            ),
          ],
        ),
      ),
    );
  }

  ///构建数字
  Widget _buildNum(BuildContext context, int time, RoundType type,
      {ChineseCalendar chTime}) {
    switch (type) {
      case RoundType.sed:
      case RoundType.min:
        return AnimatedRotation(
          turn: 6 * time / 360,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: [
            const RoundNum(type: RoundType.sed),
            const RoundNum(type: RoundType.min),
          ][type.index],
        );

        break;
      case RoundType.hour:
        return AnimatedRotation(
          turn: 15 * time / 360,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: const RoundNum(type: RoundType.hour),
        );
        break;
      case RoundType.day:
        final DateTime _date = DateTime.now();

        ///本月天数
        final int _day = DateTime(_date.year, _date.month + 1, 0).day;

        return AnimatedRotation(
          turn: (360 / _day) * time / 360,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: const RoundNum(type: RoundType.day),
        );
        break;
      case RoundType.month:
        return AnimatedRotation(
          turn: 30 * time / 360,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: const RoundNum(type: RoundType.month),
        );
        break;
      case RoundType.year:
        return BlocBuilder<AppBloc, AppMag>(
          buildWhen: (p, c) => p.showChineseCalender != c.showChineseCalender,
          builder: (context, app) {
            return RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: '$time',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OS',
                  fontSize: 60.sp,
                ),
                children: [
                  app.showChineseCalender
                      ? TextSpan(
                          text: '\n${chTime.lubarmonth}${chTime.lunarday}',
                          style: TextStyle(
                            color: Theme.of(context).dividerColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.sp,
                          ),
                        )
                      : TextSpan(),
                ],
              ),
            );
          },
        );

        break;
      default:
        break;
    }

    return Container();
  }
}
