import 'package:flutter/material.dart';
import 'package:lite_clock/util/app_util.dart';

///指针类型
///* [year] 年
///* [month] 月
///* [day] 日
///* [hour] 时
///* [min] 分
///* [sed] 秒
enum RoundType { sed, min, hour, day, month, year }

final double _fontSize = 38.sp;

///环形时钟分秒
class RoundNum extends StatelessWidget {
  const RoundNum({Key key, this.type = RoundType.sed}) : super(key: key);

  ///环类型
  final RoundType type;

  @override
  Widget build(BuildContext context) {
    return [
      _buildSed(context),
      _buildMin(context),
      _buildHour(context),
      _buildDay(context),
      _buildMon(context),
      _buildYear(context),
    ][type.index];
  }

  ///构建秒
  Widget _buildSed(BuildContext context) {
    final double _sizeSed = 1000.px;

    final List<Widget> _num = Iterable<int>.generate(30)
        .toList()
        .map((i) => RotationTransition(
              turns: AlwaysStoppedAnimation(-6 * i / 360),
              child: DefaultTextStyle(
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: _fontSize,
                  fontFamily: 'OS',
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RotatedBox(quarterTurns: 2, child: Text('${i + 30}')),
                    Text('$i'),
                  ],
                ),
              ),
            ))
        .toList();

    return SizedBox(
      width: _sizeSed,
      height: _sizeSed,
      child: Stack(
        alignment: Alignment.center,
        children: _num,
      ),
    );
  }

  ///构建分钟
  Widget _buildMin(BuildContext context) {
    final double _sizeMin = 850.px;

    final List<Widget> _num = Iterable<int>.generate(30)
        .toList()
        .map((i) => RotationTransition(
              turns: AlwaysStoppedAnimation(-6 * i / 360),
              child: DefaultTextStyle(
                style: TextStyle(
                  color: Theme.of(context).dividerColor,
                  fontWeight: FontWeight.bold,
                  fontSize: _fontSize,
                  fontFamily: 'OS',
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RotatedBox(quarterTurns: 2, child: Text('${i + 30}')),
                    Text('$i'),
                  ],
                ),
              ),
            ))
        .toList();

    return SizedBox(
      width: _sizeMin,
      height: _sizeMin,
      child: Stack(
        alignment: Alignment.center,
        children: _num,
      ),
    );
  }

  ///构建小时
  Widget _buildHour(BuildContext context) {
    final double _sizeHour = 700.px;

    final List<Widget> _num = Iterable<int>.generate(12)
        .toList()
        .map((i) => RotationTransition(
              turns: AlwaysStoppedAnimation(-15 * i / 360),
              child: DefaultTextStyle(
                style: TextStyle(
                  color: Theme.of(context).dividerColor,
                  fontWeight: FontWeight.bold,
                  fontSize: _fontSize,
                  fontFamily: 'OS',
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RotatedBox(
                      quarterTurns: 2,
                      child: Text('${i + 12}'),
                    ),
                    Text('$i'),
                  ],
                ),
              ),
            ))
        .toList();

    return SizedBox(
      width: _sizeHour,
      height: _sizeHour,
      child: Stack(
        alignment: Alignment.center,
        children: _num,
      ),
    );
  }

  ///构建天
  Widget _buildDay(BuildContext context) {
    final double _sizeMonth = 550.px;

    final DateTime _date = DateTime.now();

    ///本月天数
    final int _day = DateTime(_date.year, _date.month + 1, 0).day;

    final List<Widget> _num = Iterable<int>.generate(_day)
        .toList()
        .map((i) => RotationTransition(
              turns: AlwaysStoppedAnimation(-(360 / _day) * i / 360),
              child: DefaultTextStyle(
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: _fontSize,
                  fontFamily: 'OS',
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Text('$i'),
                  ],
                ),
              ),
            ))
        .toList();

    return SizedBox(
      width: _sizeMonth,
      height: _sizeMonth,
      child: Stack(
        alignment: Alignment.center,
        children: _num,
      ),
    );
  }

  ///构建月
  Widget _buildMon(BuildContext context) {
    final double _sizeMonth = 400.px;

    final List<Widget> _num = Iterable<int>.generate(6)
        .toList()
        .map((i) => RotationTransition(
              turns: AlwaysStoppedAnimation(-30 * i / 360),
              child: DefaultTextStyle(
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: _fontSize,
                  fontFamily: 'OS',
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RotatedBox(
                      quarterTurns: 2,
                      child: Text('${i + 6}'),
                    ),
                    Text('$i'),
                  ],
                ),
              ),
            ))
        .toList();

    return SizedBox(
      width: _sizeMonth,
      height: _sizeMonth,
      child: Stack(
        alignment: Alignment.center,
        children: _num,
      ),
    );
  }

  ///构建年
  Widget _buildYear(BuildContext context) {
    return Container();
  }
}
