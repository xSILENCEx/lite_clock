import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/app_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/util/app_util.dart';

///日期信息
class DateInfo extends StatelessWidget {
  const DateInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 900.px,
      padding: EdgeInsets.only(left: 36),
      child: BlocBuilder<TimeBloc, TimeMag>(
        buildWhen: (p, c) =>
            p.nowTime.year != c.nowTime.year ||
            p.nowTime.month != c.nowTime.month ||
            p.nowTime.day != c.nowTime.day,
        builder: (_, time) {
          context.bloc<TimeBloc>().getChCalendar();

          return BlocBuilder<AppBloc, AppMag>(
            builder: (context, app) {
              return RichText(
                text: TextSpan(
                  text:
                      '${time.nowTime.year}.${time.nowTime.month}.${time.nowTime.day}',
                  style: TextStyle(
                    fontFamily: 'UN',
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor,
                    fontSize: 40.sp,
                    height: 1,
                  ),
                  children: [
                    app.showChineseCalender
                        ? TextSpan(
                            text:
                                '  ${time.calendar.lubarmonth}${time.calendar.lunarday}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).dividerColor,
                              fontSize: 24.sp,
                            ),
                          )
                        : TextSpan(),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
