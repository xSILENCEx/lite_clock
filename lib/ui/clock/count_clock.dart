import 'package:count_by_clock/count_by_clock.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/ui/clock/date_info.dart';

import 'package:lite_clock/util/app_util.dart';

///点阵风格动画时钟
class CountClock extends StatelessWidget {
  const CountClock({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const DateInfo(),
        SizedBox(
          width: 900.px,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.hour != c.nowTime.hour,
                builder: (_, time) => _buildNum(context, time.nowTime.hour),
              ),
              _buildDot(context),
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.minute != c.nowTime.minute,
                builder: (_, time) => _buildNum(context, time.nowTime.minute),
              ),
              _buildDot(context),
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.second != c.nowTime.second,
                builder: (_, time) => _buildNum(context, time.nowTime.second),
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///构建数字
  Widget _buildNum(BuildContext context, int number) {
    final double _clockSize = 60.px;

    return CountByClock(
      number,
      digitCount: 2,
      tickColor: Theme.of(context).primaryColor,
      baseColor: Colors.transparent,
      clockArea: _clockSize,
      tickThickness: 14.px,
      hideTick: true,
      flatStyle: true,
      curve: Curves.ease,
    );
  }

  ///构建分割点
  Widget _buildDot(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 14.px,
          height: 50.px,
          decoration: BoxDecoration(
            color: Theme.of(context).dividerColor,
            borderRadius: BorderRadius.circular(50),
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: 14.px,
          height: 50.px,
          decoration: BoxDecoration(
            color: Theme.of(context).dividerColor,
            borderRadius: BorderRadius.circular(50),
          ),
        ),
      ],
    );
  }
}
