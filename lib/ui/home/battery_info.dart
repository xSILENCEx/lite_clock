import 'dart:async';

import 'package:battery/battery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/animated_scale.dart';

///电池信息
class BatteryInfo extends StatefulWidget {
  const BatteryInfo({Key key}) : super(key: key);

  @override
  _BatteryInfoState createState() => _BatteryInfoState();
}

class _BatteryInfoState extends State<BatteryInfo> {
  ///电池对象
  Battery _battery;

  ///电量
  int _level = 0;

  ///电池状态枚举
  BatteryState _batteryState;

  ///电池颜色
  Color _color;

  ///事件流
  StreamSubscription<BatteryState> _batteryStateSubscription;

  @override
  void initState() {
    super.initState();
    _battery = Battery();

    _batteryState = BatteryState.discharging;

    _color = Colors.green;

    Future.delayed(const Duration(seconds: 0), () async {
      _level = await _battery.batteryLevel;
      setState(() {});
    });

    _batteryStateSubscription =
        _battery.onBatteryStateChanged.listen(_onStateChange);
  }

  @override
  void dispose() {
    if (_batteryStateSubscription != null) {
      _batteryStateSubscription.cancel();
    }

    super.dispose();
  }

  ///电池状态变化
  _onStateChange(BatteryState state) async {
    if (_batteryState != state || _level != await _battery.batteryLevel) {
      _batteryState = state;
      _level = await _battery.batteryLevel;

      LC.log('电池状态：$_batteryState');
      LC.log('电池电量：$_level');

      if (_level < 10) {
        _color = Colors.red;
      } else if (_level < 50) {
        _color = Colors.amber;
      } else {
        _color = Colors.green;
      }

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(width: 80.px),
        Container(
          width: 40.px,
          height: 40.px,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: _color, width: 2.px),
          ),
        ),
        AnimatedScale(
          scale: _batteryState == BatteryState.charging ? 1 : 0,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: Container(
            width: 40.px,
            height: 40.px,
            alignment: Alignment.center,
            child: Icon(
              Ionicons.ios_flash,
              color: _color,
              size: 20.px,
            ),
          ),
        ),
        AnimatedPositioned(
          left: _batteryState == BatteryState.charging ? 40.px : 0,
          width: 40.px,
          height: 40.px,
          curve: Curves.ease,
          duration: const Duration(milliseconds: 500),
          child: Container(
            width: 40.px,
            height: 40.px,
            alignment: Alignment.center,
            child: Text(
              '$_level',
              style: TextStyle(
                color: _color,
                fontWeight: FontWeight.bold,
                fontFamily: 'OS',
                fontSize: 16.sp,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
