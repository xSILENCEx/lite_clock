import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:lite_clock/bloc/app_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';

import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/lc_switch.dart';

import 'change_color_panel.dart';

///设置界面
class SettingPage extends StatelessWidget {
  final Function onSetTap;

  const SettingPage({Key key, @required this.onSetTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 40.px, left: 40.px),
          child: IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(
              Feather.arrow_left,
              size: 40.px,
              color: Colors.white.withOpacity(0.3),
            ),
            onPressed: onSetTap,
          ),
        ),
        Expanded(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 60.px, vertical: 40.px),
            physics: LC.LcPhysics,
            children: [
              Wrap(
                spacing: 20.px,
                runSpacing: 20.px,
                children: [
                  BlocBuilder<AppBloc, AppMag>(
                    builder: (context, app) {
                      return LcSwitch(
                        onChange: (v) {
                          context.bloc<AppBloc>().changeBatteryState(v);
                        },
                        info: '显示电池电量',
                        autoSelect: app.showBattery,
                      );
                    },
                  ),
                  BlocBuilder<AppBloc, AppMag>(
                    builder: (context, app) {
                      return LcSwitch(
                        onChange: (v) {
                          context.bloc<AppBloc>().changeCalenderState(v);
                          if (v) {
                            context.bloc<TimeBloc>().getChCalendar();
                            LC.log('显示农历');
                          }
                        },
                        info: '显示农历',
                        autoSelect: app.showChineseCalender,
                      );
                    },
                  ),
                  BlocBuilder<AppBloc, AppMag>(
                    builder: (context, app) {
                      return LcSwitch(
                        onChange: (v) {
                          context.bloc<AppBloc>().changeMoveState(v);
                        },
                        info: '自动更换位置(防止OLED烧屏)',
                        autoSelect: app.isMoveClock,
                      );
                    },
                  ),
                  _buildChangeColorBtn(context),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///选择颜色
  _selectColor(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (_) => const ChangeColorPanel(),
    );
  }

  ///修改颜色按钮
  Widget _buildChangeColorBtn(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.zero,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.px)),
      onPressed: () async => await _selectColor(context),
      child: Container(
        width: 200.px,
        height: 200.px,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.px),
          border: Border.all(color: Colors.white.withOpacity(0.5), width: 2.px),
        ),
        child: Padding(
          padding: EdgeInsets.all(20.px),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BlocBuilder<AppBloc, AppMag>(
                buildWhen: (p, c) =>
                    p.clockColor != c.clockColor || p.dotColor != c.dotColor,
                builder: (context, app) {
                  return RichText(
                    text: TextSpan(
                      text: '12',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OS',
                        fontSize: 30.sp,
                      ),
                      children: [
                        TextSpan(
                          text: ':',
                          style: TextStyle(
                            color: Theme.of(context).dividerColor,
                          ),
                        ),
                        TextSpan(text: '00'),
                        TextSpan(
                          text: ':',
                          style: TextStyle(
                            color: Theme.of(context).dividerColor,
                          ),
                        ),
                        TextSpan(text: '00'),
                      ],
                    ),
                  );
                },
              ),
              Text(
                '修改时钟颜色',
                style: TextStyle(
                  color: Colors.white.withOpacity(0.5),
                  fontSize: 20.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
