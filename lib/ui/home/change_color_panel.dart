import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:lite_clock/bloc/app_bloc.dart';

import 'package:lite_clock/util/app_util.dart';

///色彩选择器,修改时钟颜色
class ChangeColorPanel extends StatefulWidget {
  const ChangeColorPanel({Key key}) : super(key: key);

  @override
  _ChangeColorPanelState createState() => _ChangeColorPanelState();
}

class _ChangeColorPanelState extends State<ChangeColorPanel> {
  ///时钟颜色
  Color _pickClockColor = Colors.blue;

  ///点缀颜色
  Color _pickDotColor = Colors.blue;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 0), () {
      _pickClockColor =
          Color(BlocProvider.of<AppBloc>(context).getAppSetting.clockColor);
      _pickDotColor =
          Color(BlocProvider.of<AppBloc>(context).getAppSetting.dotColor);

      setState(() {});
    });
  }

  ///确认修改颜色
  _check() async {
    BlocProvider.of<AppBloc>(context).changeClockColor(_pickClockColor.value);
    BlocProvider.of<AppBloc>(context).changeDotColor(_pickDotColor.value);

    LC.t('修改成功');

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: MediaQuery.of(context).size.width - 180.px,
          height: MediaQuery.of(context).size.height - 80.px,
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(20.px),
            border: Border.all(color: Colors.white, width: 1),
          ),
          child: Column(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: '12',
                        style: TextStyle(
                          color: _pickClockColor,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OS',
                          fontSize: 110.sp,
                        ),
                        children: [
                          TextSpan(
                              text: ':',
                              style: TextStyle(color: _pickDotColor)),
                          TextSpan(text: '00'),
                          TextSpan(
                              text: ':',
                              style: TextStyle(color: _pickDotColor)),
                          TextSpan(text: '00'),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FlatButton(
                          padding: EdgeInsets.zero,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () async => await _check(),
                          child: Text(
                            '确认',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        FlatButton(
                          padding: EdgeInsets.zero,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            '取消',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: 1,
                width: double.infinity,
                color: Colors.white,
              ),
              SizedBox(
                height: 200.px,
                child: Row(
                  children: [
                    Expanded(
                      child: MaterialPicker(
                        pickerColor: _pickClockColor,
                        onColorChanged: (c) {
                          setState(() {
                            _pickClockColor = c;
                          });
                        },
                        enableLabel: false,
                      ),
                    ),
                    Container(
                        width: 1, height: double.infinity, color: Colors.white),
                    Expanded(
                      child: MaterialPicker(
                        pickerColor: _pickDotColor,
                        onColorChanged: (c) {
                          setState(() {
                            _pickDotColor = c;
                          });
                        },
                        enableLabel: false,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
