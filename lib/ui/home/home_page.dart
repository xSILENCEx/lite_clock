import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:lite_clock/bloc/app_bloc.dart';
import 'package:lite_clock/bloc/time_bloc.dart';
import 'package:lite_clock/ui/clock/count_clock.dart';
import 'package:lite_clock/ui/clock/round_clock/round_clock.dart';
import 'package:lite_clock/ui/clock/simple_clock.dart';
import 'package:lite_clock/ui/clock/slide_clock.dart';
import 'package:lite_clock/util/app_util.dart';
import 'package:lite_clock/widgets/clock_background.dart';

import 'battery_info.dart';

///主页
class HomePage extends StatefulWidget {
  ///点击设置按钮回调
  final Function onSetTap;

  const HomePage({
    Key key,
    @required this.onSetTap,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  int _moveIndex = 0;

  ///移动时钟，防止烧屏
  _move() {
    if (_moveIndex >= 2) {
      _moveIndex = 0;
    } else {
      _moveIndex++;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    LC.log('build HomePage', type: 1);

    final List<double> _pos = [40.px, 0, -40.px];

    return ClockBackground(
      child: BlocBuilder<AppBloc, AppMag>(
        buildWhen: (p, c) => p.isMoveClock != c.isMoveClock,
        builder: (context, app) {
          return Stack(
            alignment: Alignment.topRight,
            children: [
              BlocBuilder<TimeBloc, TimeMag>(
                  buildWhen: (p, c) =>
                      p.nowTime.minute != c.nowTime.minute &&
                      c.nowTime.minute % 3 == 0,
                  builder: (c, time) {
                    _move();

                    return AnimatedPositioned(
                      curve: Curves.ease,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      top: app.isMoveClock ? _pos[_moveIndex] : 0,
                      right: app.isMoveClock ? _pos[_moveIndex] : 0,
                      duration: const Duration(seconds: 1),
                      child: const ClockBody(),
                    );
                  }),
              BlocBuilder<TimeBloc, TimeMag>(
                buildWhen: (p, c) => p.nowTime.minute != c.nowTime.minute,
                builder: (c, time) {
                  return AnimatedPadding(
                    curve: Curves.ease,
                    padding: EdgeInsets.only(
                      //每10分钟更换图标位置
                      right: time.nowTime.minute % 10 == 0 && app.isMoveClock
                          ? 80.px
                          : 40.px,
                      left: time.nowTime.minute % 10 == 0 && app.isMoveClock
                          ? 80.px
                          : 40.px,
                      top: 40.px,
                    ),
                    duration: const Duration(seconds: 1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        BlocBuilder<AppBloc, AppMag>(
                          buildWhen: (p, c) => p.showBattery != c.showBattery,
                          builder: (context, app) => app.showBattery
                              ? const BatteryInfo()
                              : SizedBox(width: 0, height: 0),
                        ),
                        IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(
                            Feather.settings,
                            size: 40.px,
                            color: Colors.white.withOpacity(0.2),
                          ),
                          onPressed: widget.onSetTap,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ClockBody extends StatelessWidget {
  const ClockBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const List<Widget> _clocks = [
      const SlideClock(),
      const CountClock(),
      const SimpleClock(),
      const RoundClock(),
    ];

    return BlocBuilder<AppBloc, AppMag>(
      buildWhen: (p, c) => false,
      builder: (context, app) {
        return PageView.builder(
          controller: PageController(initialPage: app.clockIndex),
          scrollDirection: Axis.vertical,
          itemCount: _clocks.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (_, index) {
            return ClipRect(child: _clocks[index]);
          },
          onPageChanged: (i) {
            context.bloc<AppBloc>().changeClockIndex(i);
          },
        );
      },
    );
  }
}
