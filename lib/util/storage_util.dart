import 'dart:convert';
import 'dart:io';
import 'package:lite_clock/util/app_util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

///数据存取工具
class StorageUtil {
  ///获取bool
  static Future<bool> getBool(key) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getBool(key);
    } catch (e) {
      await setBool(key, false);
      LC.log('getBool error:$e', type: 2);
      return false;
    }
  }

  ///获取本地字符串
  static Future<String> getStr(key) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {
      LC.log('getStr error:$e', type: 2);
      setStr(key, '');
      return null;
    }
  }

  ///获取本地整型
  static Future<int> getInt(key) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getInt(key);
    } catch (e) {
      LC.log('getInt error:$e', type: 2);
      setInt(key, 0);
      return 0;
    }
  }

  ///获取键值对列表
  static Future<List<String>> getStrList(key) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getStringList(key);
    } catch (e) {
      LC.log('getStrList error:$e', type: 2);
      setStrList(key, []);
      return [];
    }
  }

  ///保存bool
  static setBool(key, bool value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setBool(key, value);

      LC.log('成功缓存:$key');
    } catch (e) {
      LC.log('setBool error:$e', type: 2);
      return null;
    }
  }

  ///保存字符串
  static setStr(key, String value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString(key, value);
      LC.log('成功缓存:$key');
    } catch (e) {
      LC.log('setStr error:$e', type: 2);
      return null;
    }
  }

  ///保存字符串列表
  static setStrList(key, List<String> value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setStringList(key, value);
    } catch (e) {
      LC.log('setStrList error:$e', type: 2);
      return null;
    }
  }

  ///保存整型
  static setInt(key, int value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.setInt(key, value);
      LC.log('成功缓存:$key');
    } catch (e) {
      LC.log('setInt error:$e', type: 2);
      return null;
    }
  }

  ///删除键值对
  static delete(key) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      prefs.remove(key);
    } catch (e) {
      LC.log('delete error:$e', type: 2);
      return null;
    }
  }

  ///读取文件
  static readFile(String fileName, String path, String info,
      {String type = 'map'}) async {
    try {
      if (fileName == null || fileName.trim().length == 0) {
        fileName = "defaultuser";
      }
      final File file =
          await _localFile('${await _localPath()}/$path', fileName);
      final String str = await file.readAsString();

      LC.log(
          '$info:读取$path/$fileName---文件大小:${(file.lengthSync() / 1024 / 1024).toStringAsFixed(2)}m');

      return json.decode(str);
    } catch (err) {
      LC.log('$path/$fileName 没有这个文件(￣﹃￣)');

      //没有文件就创建一个
//      switch (type) {
//        case 'map':
//          await writeFile({}, fileName, path);
//          break;
//        case 'string':
//          await writeFile('', fileName, path);
//          break;
//        case 'list':
//          await writeFile([], fileName, path);
//          break;
//      }

      return null;
    }
  }

  ///写入文件
  static writeFile(obj, String fileName, String path, String info) async {
    try {
      if (fileName == null || fileName.trim().length == 0) {
        fileName = "defaultuser";
      }
      final File file =
          await _localFile('${await _localPath()}/$path', fileName);

      LC.log('$info:写入$path/$fileName成功');

      return await file.writeAsString(json.encode(obj));
    } catch (err) {
      LC.log('$fileName 写入数据出错 : $err', type: 2);
      writeFile(obj, fileName, path, '写入文件');
      return null;
    }
  }

  ///删除文件
  static Future<bool> deleteFile(String fileName, String path) async {
    try {
      final File file =
          await _localFile('${await _localPath()}/$path', fileName);
      file.delete();
      LC.log('删除文件$fileName');
      return true;
    } catch (err) {
      LC.log('$fileName 删除出错 : $err', type: 2);
      return null;
    }
  }

  ///文件位置引用
  static Future<File> _localFile(path, String filename) async {
    return await File('$path/$filename').create(recursive: true);
  }

  ///使用 path_provider 查找本地路径
  static Future<String> _localPath() async {
    try {
      var appDocDir = await getApplicationDocumentsDirectory();
      final String appDocPath = appDocDir.path;
      return appDocPath;
    } catch (err) {
      LC.log('查找本地路径出错 : $err', type: 2);
    }

    return '';
  }
}
