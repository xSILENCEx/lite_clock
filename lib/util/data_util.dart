import 'package:intl/intl.dart';

///处理数据的工具
class DataUtil {
  ///获取YYYY-MM-DD类型的数据
  static String getFormDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }
}
