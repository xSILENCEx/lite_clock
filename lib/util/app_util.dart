import 'package:ansicolor/ansicolor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:developer' as dev;

class LC {
  ///滚动物理封装
  static const LcPhysics =
      const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics());

  ///打印日志
  ///* [0] 信息
  ///* [1] 警告
  ///* [2] 错误
  ///默认为`1`：信息
  static void log(dynamic content, {int type = 0}) {
    assert(() {
      AnsiPen _pen = AnsiPen()..white(bold: true);
      AnsiPen _leadPen;
      String _leading;

      final String _time = DateTime.now().toString().split('.')[0];

      switch (type) {
        case 0:
          _leading = "[INFO] ";
          _leadPen = AnsiPen()..green();
          break;
        case 1:
          _leading = "[WARNING] ";
          _leadPen = AnsiPen()..yellow();
          break;
        case 2:
          _leading = "[ERROR] ";
          _pen = AnsiPen()..red(bold: true);
          _leadPen = AnsiPen()..red();
          break;
        default:
          _leading = "[OTHER] ";
          _leadPen = AnsiPen()..white();
          break;
      }

      dev.log(_leadPen(_leading) + _pen(content ?? ''), name: _time);

      return true;
    }());
  }

  ///显示Toast
  static Future<void> t(
    String msg, {
    Toast toastLength,
    int timeInSecForIosWeb = 1,
    double fontSize = 16.0,
    ToastGravity gravity = ToastGravity.CENTER,
    Color backgroundColor,
    Color textColor,
    bool webShowClose = false,
    dynamic webBgColor = "linear-gradient(to right, #00b09b, #96c93d)",
    dynamic webPosition = "right",
  }) async {
    await Fluttertoast.cancel();
    await Fluttertoast.showToast(
      msg: msg ?? '',
      gravity: gravity,
    );
  }

  ///初始化屏幕
  ///* `standardWidth` 设计稿屏幕宽度，默认`1080`
  static void initScreen(BuildContext context, {double standardWidth = 1080}) {
    ScreenUtil.init(context);
    // SU.initialize(context, standardWidth: standardWidth);
  }

  ///隐藏状态栏
  static void hideStatusBar() {
    log('隐藏状态栏');
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  }

  ///显示状态栏
  static void showStatusBar({bool dark = true}) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(
        dark ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light);
  }

  ///权限检查与请求
  static Future<bool> checkPermission(Permission permission) async {
    final _status = await permission.status;
    bool _state = false;

    if (_status.isGranted) {
      log('权限 $permission 成功获取');
      return true;
    } else {
      await permission.request().then(
        (s) async {
          if (s == PermissionStatus.granted) {
            log('权限 $permission 成功获取');
            _state = true;
          } else {
            await openAppSettings().then(
              (b) {
                if (b) {
                  log('权限 $permission 成功获取');
                  _state = true;
                }
              },
            );
          }
        },
      );
    }

    return _state;
  }
}

///扩展`int`类型
extension IntFit on int {
  double get px {
    // return SU.setPx(this.toDouble());
    return ScreenUtil().setWidth(this.toDouble());
  }

  double get sp {
    // return SU.setRpx(this.toDouble());
    return ScreenUtil().setSp(this.toDouble());
  }
}

///扩展`double`类型
extension DoubleFit on double {
  double get px {
    // return SU.setPx(this.toDouble());
    return ScreenUtil().setWidth(this.toDouble());
  }

  double get sp {
    // return SU.setRpx(this.toDouble());
    return ScreenUtil().setSp(this.toDouble());
  }
}
