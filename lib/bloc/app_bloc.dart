import 'package:bloc/bloc.dart';
import 'package:lite_clock/util/storage_util.dart';

///应用事件
enum AppEnv { reload }

class AppMag {
  ///时钟颜色
  final int clockColor;

  ///点缀色
  final int dotColor;

  ///选中的时钟样式
  final int clockIndex;

  ///是否显示电池
  final bool showBattery;

  ///显示农历
  final bool showChineseCalender;

  ///是否自动移动时钟
  final bool isMoveClock;

  const AppMag({
    this.clockIndex = 0,
    this.clockColor = 0,
    this.dotColor = 0,
    this.showBattery = true,
    this.showChineseCalender = false,
    this.isMoveClock = true,
  });

  AppMag copyWith({
    int clockIndex,
    int clockColor,
    int dotColor,
    bool showBattery,
    bool showChineseCalender,
    bool isMoveClock,
  }) {
    return AppMag(
      clockIndex: clockIndex ?? this.clockIndex,
      clockColor: clockColor ?? this.clockColor,
      dotColor: dotColor ?? this.dotColor,
      showBattery: showBattery ?? this.showBattery,
      showChineseCalender: showChineseCalender ?? this.showChineseCalender,
      isMoveClock: isMoveClock ?? this.isMoveClock,
    );
  }
}

///应用管理
class AppBloc extends Bloc<AppEnv, AppMag> {
  AppMag _appMag = AppMag(
    clockColor: 0xFF9F7CFF,
    dotColor: 0xFFF89037,
  );

  AppBloc(AppMag initialState) : super(initialState);

  ///获取状态管理对象
  AppMag get getAppSetting => _appMag;

  ///修改时钟颜色
  changeClockColor(int color) async {
    _appMag = _appMag.copyWith(clockColor: color);
    await StorageUtil.setInt('clockColor', color);

    add(AppEnv.reload);
  }

  ///修改点缀颜色
  changeDotColor(int color) async {
    _appMag = _appMag.copyWith(dotColor: color);
    await StorageUtil.setInt('dotColor', color);

    add(AppEnv.reload);
  }

  ///修改时钟样式索引
  changeClockIndex(int index) async {
    await StorageUtil.setInt('clockIndex', index);
  }

  ///修改电池状态
  changeBatteryState(bool s) async {
    await StorageUtil.setBool('showB', s);

    _appMag = _appMag.copyWith(showBattery: s);
    add(AppEnv.reload);
  }

  ///修改农历显示状态
  changeCalenderState(bool s) async {
    await StorageUtil.setBool('showC', s);

    _appMag = _appMag.copyWith(showChineseCalender: s);
    add(AppEnv.reload);
  }

  ///修改时钟自动移动的状态
  changeMoveState(bool s) async {
    await StorageUtil.setBool('move', s);
    _appMag = _appMag.copyWith(isMoveClock: s);
    add(AppEnv.reload);
  }

  @override
  Stream<AppMag> mapEventToState(AppEnv event) async* {
    yield _appMag;
  }
}
